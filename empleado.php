<?php

/**
 * Created by PhpStorm.
 * User: chris
 * Date: 2/11/18
 * Time: 03:49 PM
 */

include_once ('librerias/php/tribunet/class.sxMailer.php');

use Utils\Form\form;
use Utils\Form\fieldset;

class Base_empleado extends configBase
{
    public $isAdmin = true;
    public $objEmpleado;
    public $objAreas;

    public function __construct(){
        $this->objEmpleado = new GstBaseEmpleados();
        $this->objAreas = new GstBaseAreas();
    }

    public function main(){
        $this->Lst();
    }
    public function Lst(){
        $this->title = "Lista de empleados";
        $this->view = "empleado.main";
        $this->vData['empleados'] = $this->objEmpleado->getEmpleados();
        $this->render();
    }

    public function pruebas(){
        echo "<hr>Carpeta del cliente activo: ".getClienteFolder();
        echo "<hr>Carpeta media del cliente activo desde Info: ".getClienteMedia();

        echo "<hr>Cliente Id: ".getClienteId();
        echo "<hr>Cliente Nombre: ".getClienteNombre();
        echo "<hr>Usuario Que inicio sesion: ".getUserId();
        echo "<hr>Logion del usuario: ".getUserLogin();

        echo "<hr>Nombre del usuario que inicio sesion: ".getUserNombre();

        echo "<hr> info usuario que ha iniciado sesion: ";
        print_r($_SESSION['infoUsuario']);

        echo "<hr>Comunidad: ".getComunidadActual();
    }

    //FFORMULARIO CREAR EMPLEADO
    public function create(){
        $this->title = "Nuevo empleado";
        $empleado = $this->objEmpleado->getNewEmpleado();

        $form = new form("Base", "empleado", 'createProcess');
        $this->configFormEmpleado($empleado, $form);

        $this->vData['form'] = $form->dsp();
        $this->view = "empleado.create";
        $this->render();
    }




    //Formulario actualizar empleado
    public function update(){
        $this->title = "Editar empleado";
        $empleado = $this->objEmpleado->getEmpleado($_GET['id']);

        $form = new form("Base", "empleado", 'updateProcess');
        $this->configFormEmpleado($empleado, $form);

        $this->vData['form'] = $form->dsp();
        $this->view = "empleado.create";
        $this->render();
    }




    //EJECUCION DEL FORMULARIO CREAR EMPLEADO
    public function createProcess(){
        $this->createUpdateProcess();
    }

    //Ejecucion de formulario editar empleado
    public function updateProcess(){
        $this->createUpdateProcess();
    }

    //Ejecucion Eliminar Empleado
    public function deleteProcess(){
        if (!isset($_GET['id']))
            die();

        $empleado = $this->objEmpleado->getEmpleado($_GET['id']);
        try {
            if (!$empleado->delete())
                setMsg($empleado->getMsgError(), SX_ERROR, true);
            else
                setMsg('Eliminada con éxito', SX_OK, true);
        }catch (Exception $e) {
            setMsg('Ocurrió un error al eliminar', SX_ERROR, true);
        }
        redirect(genUrl("Base", "empleado"));
    }


    private function createUpdateProcess(){
        if (!$_POST['TnBaEm'])
            die();

        $msg="Creado";
        if ($_GET['lFunction']=='updateProcess' && isset($_GET['id'])) {
            $msg="Editado";
            $empleado = $this->objEmpleado->getEmpleado($_GET['id']);
        }else
            $empleado = $this->objEmpleado->getNewEmpleado();

        $empleado->nombre=$_POST['TnBaEm']['nombre'];
        $empleado->email=$_POST['TnBaEm']['email'];
        $empleado->sexo=$_POST['TnBaEm']['sexo'];
        $empleado->area_id=$_POST['TnBaEm']['area_id'];
        $empleado->descripcion=$_POST['TnBaEm']['descripcion'];
        $empleado->boletin=(isset($_POST['TnBaEm']['boletin']))?1:0;

        $empleado->save();

        try {
            if(!$empleado->save()){
                setMsg($empleado->getMsgError(), SX_ERROR);
            }
            else{
                setMsg('Empleado '.$msg.' exitosamente', SX_OK, true);
            }
        } catch (Exception $e) {
            setMsg('Ocurri&oacute;', SX_ERROR, true);
        }
        redirect(genUrl("Base", "empleado"));
    }


    /*CONFIG*/
    private function configFormEmpleado($empleado, $form){
        $form->id = 'formEmpleado';
        $form->btnSubmitText = 'Nuevo empleado';
        //$form->files = true;
        $fieldSet = new fieldset('Datos basicos');

        $fieldSet->addRows(
            [
                $empleado->getField("nombre"),
                $empleado->getField("email")
            ]
        );
        $fieldSet->addRows(
            [
                $empleado->getField("sexo"),
                $empleado->getField("area_id")
            ]
        );
        $fieldSet->addRows(
            [
                $empleado->getField("descripcion"),
            ]
        );
        $fieldSet->addRows(
            [$empleado->getField("boletin") ]
        );

        $form->addFieldset($fieldSet);
    }


    public function enviarEmail(){
        $sxMailerObj = new sxMailer();
        $res = $sxMailerObj->send("chvc2004@yahoo.com", "ChrisVale", "prueba MSG", "El mensaje", "Chris", "cvalenciac@nexura.com");
        print_r($res);
    }
}