
@extends(PATH_VIEW_LAYOUT."adminLayout")

@section("content")
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <div class="container">
        <br/>
        <div class="row">
            <div class="col-sm text-right">
                <a href="{{genUrl('Base','empleado','create')}}" class="iframe btn btn-primary"><i class="fas fa-user-plus"></i> Crear</a>
            </div>

        </div>
        <br/>
        <table class="table table-hover table-striped">
            <thead class="thead-dark">
            <tr>
                <th><i class="fas fa-user"></i> Nombre</th>
                <th><i class="fas fa-envelope-square"></i> Email</th>
                <th>Sexo</th>
                <th>Area</th>
                <th>Boletín</th>
                <th>Modificar</th>
                <th>Eliminar</th>
            </tr>
            </thead>
            <tbody>
            <?php  foreach ($empleados as $empleado) { ?>
            <tr id="empl<?=$empleado->id?>">
                <td><?= $empleado->nombre ?></td>
                <td><?= $empleado->email ?></td>
                <td><?= $empleado->sexo ?></td>
                <td><?= $empleado->area->nombre ?></td>
                <td><?= ($empleado->boletin)?"Si":"No" ?></td>
                <td> <a href="{{genUrl("Base","empleado","update",['id'=>$empleado->id])}}" class="iframe btn btn-sm btn-info"><i class="fas fa-edit"></i> Editar</a> </td>

                <td> <a href="{{genUrl("Base","empleado","deleteProcess",['id'=>$empleado->id])}}" class="btnEliminar btn btn-sm btn-danger"><i class="fas fa-edit"></i> Eliminar</a> </td>

            </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>

@stop