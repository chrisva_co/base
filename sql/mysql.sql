/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  christian valencia cuero
 * Created: 02/11/2018
 * SHOW INDEX FROM tn_demo_empleados;
 */

CREATE TABLE IF NOT EXISTS `tn_base_areas` (
  `id` int (11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL
);

CREATE TABLE `tn_base_empleados` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `email` text NOT NULL,
  `sexo` char(1) NOT NULL,
  `area_id` int(11) NOT NULL,
  FOREIGN KEY (`area_id`) REFERENCES `tn_demo_areas`(`id`),
  `boletin` int(11) NOT NULL,
  `descripcion` text NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




--
-- Índices para tablas volcadas
--
