<?php
/**
* 
* @package NombreModulo
* @copyright Copyright {@link http://www.nexura.com nexura} 2017
* @version NombreModulo v1.0
*
* @author Ing. Jhonnatham Samir Salgado <jssalgado@nexura.com>
*/
class Base_admin  extends configBase{


	public $isAdmin = true;

	function main(){
	   //echo "Hola Mundo";
		$this->title = 'Page title';
		$this->view = 'admin.main';
		$this->vData = [];
		$this->render();
	} // main
} // class
?>
