<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 6/11/18
 * Time: 10:53 AM
 */

class GstBaseAreas
{
    public function getArea($id){
        return TnBaseAreas::find($id);
    }

    public function getAreas(){
        return TnBaseAreas::all();
    }

    public function getNewArea(){
        return new TnBaseAreas();
    }
}