<?php

class TnBaseAreas extends \Utils\eloquent\modelORM{

    protected $table = "tn_base_areas";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'nombre'
    ];

    protected $fields = [
        'id' => [
            'type' => 'number'
        ],
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre de Area'
        ]
    ];
}