<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 2/11/18
 * Time: 03:35 PM
 */

class TnBaseEmpleados extends \Utils\eloquent\modelORM
{
    protected $table = "tn_base_empleados";
    public $timestamps = false;


    protected $fillable = ['id',
        'nombre',
        'email',
        'sexo',
        'area_id',
        'boletin',
        'descripcion'];

    protected $fields = [
        'nombre' => [
            'type' => 'text',
            'label' => 'Nombre completo',
            'dspOrientation' => 'v',
            'html'=>[
                'placeholder'=>'Nombre completo'
            ],
            'rules'=>[
                'maxLength'=>60,
                'required'=>true
            ]
        ],
        'email' => [
            'type'=> 'email',
            'label' => 'Correo electr&oacutenico',
            'dspOrientation' => 'v',
            'rules'=>[
                'maxLength'=>60,
                'required'=>true
            ],
            'msgErrorJs'=>'El email esta mal viejo'
        ],


        'area_id' => [
            'type' => 'select',
            'label' => 'Areas',
            'dspOrientation' => 'v',
            'rules' => [
                'required'=>true,
            ],
            'html'=>[
                'size' => 3
            ]
        ],

        'sexo' => [
            'type'=> 'radio',
            'label' => 'Sexo',
            'dspOrientation' => 'v',
            'rules'=>[
                'required'=>true
            ]
        ]
        ,
        'descripcion' => [
            'type'=> 'textarea',
            'label' => 'Descripcion',
            'dspOrientation' => 'v',
            'rules'=>[
                'required'=>true
            ],
            'html'=>[
                'placeholder'=>'Escribe aqui la descripcion...'
            ]
        ] ,
        'boletin' => [
            'type'=> 'checkbox',
            'label' => 'Boletin',
            'dspOrientation' => 'v',
            'rules'=>[

            ],
            'html'=>[
                'classField'=>'text-center',
                'value'=>'1'
            ],

        ]
    ];

    public function boletinLst(){
        return [ ($this->boletin==1)?1:null => "Deseas recibir info a tu email? "];
    }

    public function sexoLst(){
        return ['M'=>'Masculino', 'F'=>'Femenino'];
    }

    public function area(){
        return $this->hasOne('TnBaseAreas','id','area_id');
    }

    public function areaIdLst(){
        return TnBaseAreas::select("id", "nombre")->lists("nombre", "id");
    }


    public function deportesPersonaLst(){
        return ['fu'=>'Fútbol','vo'=>'Voleibol','ba'=>'Baloncesto', 'na'=>'Natación'];
    }
}