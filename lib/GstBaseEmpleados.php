<?php


/**
 * Created by PhpStorm.
 * User: chris
 * Date: 2/11/18
 * Time: 03:44 PM
 */

class GstBaseEmpleados
{
    public function getEmpleado($id){
        return TnBaseEmpleados::find($id);
    }

    public function getEmpleados(){
       return TnBaseEmpleados::all();
    }

    public function getNewEmpleado(){
        return new TnBaseEmpleados();
    }
}